package com.saleservice.saleservice.model.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @author marvin tola
 */

@Entity
@Table(name = "client_table")
@PrimaryKeyJoinColumn(name = "clientid",referencedColumnName = "personid")
public class Client extends Person {

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lastpurchase")
    private Date lastpurchase;

    public Date getLastpurchase() {
        return lastpurchase;
    }

    public void setLastpurchase(Date lastpurchase) {
        this.lastpurchase = lastpurchase;
    }
}
