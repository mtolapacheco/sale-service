package com.saleservice.saleservice.model.domain;

/**
 * @author Marvin Tola
 */
public enum TypeGender {
    MALE,FEMALE
}
