package com.saleservice.saleservice.model.domain;

import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;


/**
 * @author marvin tola
 */
@Entity
@Table(name="person_table")
@Inheritance(strategy=InheritanceType.JOINED)

public abstract class Person {

    @Id
    @Column(name ="personid",nullable = false)
    @GeneratedValue(strategy =GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "email" ,length =100)
    private String email;

    @Column(name = "firstname",length = 50,nullable = false)
    private String firstname;

    @Column(name = "lastname",length = 50,nullable = false)
    private String lastname;

    @Enumerated(EnumType.STRING)
    @Column(name = "gender",length = 50,nullable = false)
    private TypeGender gender;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = "isdeleted",nullable = false)
    private Boolean isdeleted;

    @Column(name="createddate",length = 50,nullable = false)
    private Date createddate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public TypeGender getGender() {
        return gender;
    }

    public void setGender(TypeGender gender) {
        this.gender = gender;
    }

    public Boolean getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(Boolean isdeleted) {
        this.isdeleted = isdeleted;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }
}
