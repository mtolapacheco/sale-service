package com.saleservice.saleservice.input;

import com.saleservice.saleservice.model.domain.TypeGender;

import java.util.Date;

/**
 * @author marvin tola
 */
public class ClientCreateInput {

    private Date lastpurchase;

    private String email;

    private String firstname;

    private String lastname;

    private TypeGender gender;

    public Date getLastpurchase() {
        return lastpurchase;
    }

    public void setLastpurchase(Date lastpurchase) {
        this.lastpurchase = lastpurchase;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public TypeGender getGender() {
        return gender;
    }

    public void setGender(TypeGender gender) {
        this.gender = gender;
    }
}
