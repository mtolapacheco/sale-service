package com.saleservice.saleservice.service;

import com.saleservice.saleservice.input.ClientCreateInput;
import com.saleservice.saleservice.model.domain.Client;
import com.saleservice.saleservice.model.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author marvin tola
 */
@Service
@Scope("prototype")
public class ClientCreateService {

    private ClientCreateInput input;

    @Autowired
    private ClientRepository clientRepository;


    private Client client;

    public void execute(){

        Client clientInstance=composeClient();
        client=clientRepository.save(clientInstance);

    }

    private Client composeClient() {
        Client instance =new Client();
        instance.setLastpurchase(new Date());
        instance.setEmail(input.getEmail());
        instance.setFirstname(input.getFirstname());
        instance.setLastname(input.getLastname());
        instance.setGender(input.getGender());
        instance.setIsdeleted(Boolean.TRUE);
        instance.setCreateddate(new Date());
        return instance;
    }

    public ClientCreateInput getInput() {
        return input;
    }

    public void setInput(ClientCreateInput input) {
        this.input = input;
    }

    public ClientRepository getClientRepository() {
        return clientRepository;
    }

    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
