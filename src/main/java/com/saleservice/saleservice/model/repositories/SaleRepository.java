package com.saleservice.saleservice.model.repositories;

import com.saleservice.saleservice.model.domain.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author marvin tola
 */
public interface SaleRepository extends JpaRepository<Sale,Long> {
}
