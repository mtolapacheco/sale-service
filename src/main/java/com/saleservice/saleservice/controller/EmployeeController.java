package com.saleservice.saleservice.controller;

import com.saleservice.saleservice.input.EmployeeCreateInput;
import com.saleservice.saleservice.model.domain.Employee;
import com.saleservice.saleservice.service.EmployeCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author marvin tola
 */
@Api(tags ="employee",description = "Operation over employees")
@RestController
@RequestMapping("/public/employees")
@RequestScope
public class EmployeeController {

    @Autowired
    EmployeCreateService employeCreateService;

    @ApiOperation(
            value = "Create an employee"
    )
    @ApiResponses({
            @ApiResponse(
                    code = 401,
                    message = "Unauthorized to create account"
            ),
    })
    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeCreateInput input){
        employeCreateService.setInput(input);
        employeCreateService.execute();
        return employeCreateService.getEmployee();
    }

}
