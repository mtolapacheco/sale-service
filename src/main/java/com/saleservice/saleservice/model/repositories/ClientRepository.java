package com.saleservice.saleservice.model.repositories;

import com.saleservice.saleservice.model.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author marvin tola
 */
public interface ClientRepository extends JpaRepository<Client,Long> {
}
