package com.saleservice.saleservice.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * @author marvin tola
 */
@Entity
@Table(name="employee_table")
@PrimaryKeyJoinColumn(name = "employeeid",referencedColumnName = "personid")

public class Employee extends Person {



    @Column(name = "position",length = 50,nullable = false)
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

}
