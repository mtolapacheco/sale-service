package com.saleservice.saleservice.input;

import com.saleservice.saleservice.model.domain.TypeGender;

/**
 * @author marvin tola
 */
public class EmployeeCreateInput {

    private String position;

    private String firstname;

    private String lastname;

    private TypeGender gender;

    private String email;


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public TypeGender getGender() {
        return gender;
    }

    public void setGender(TypeGender gender) {
        this.gender = gender;
    }
}
