package com.saleservice.saleservice.model.repositories;

import com.saleservice.saleservice.model.domain.Detail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author marvin tola
 */
public interface DetailRepository extends JpaRepository<Detail,Long> {
}
