package com.saleservice.saleservice.model.repositories;

import com.saleservice.saleservice.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author marvin tola
 */
public interface EmployeeRepository extends JpaRepository<Employee,Long> {
}
