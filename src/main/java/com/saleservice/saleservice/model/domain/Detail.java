package com.saleservice.saleservice.model.domain;

import javax.persistence.*;

/**
 * @author marvin tola
 */
@Entity
@Table(name = "detail_table")
@Inheritance(strategy = InheritanceType.JOINED)
public class Detail {

    @Id
    @Column(name = "detailid",nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "totalproducts",nullable = false)
    private Integer totalproducts;

    @Column(name = "totalprice",nullable = false)
    private Long totalprice;

    @OneToOne(fetch = FetchType.LAZY,optional =false)
    @JoinColumn(name = "detailsaleid",referencedColumnName ="saleid",nullable = false)
    private Sale sale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalproducts() {
        return totalproducts;
    }

    public void setTotalproducts(Integer totalproducts) {
        this.totalproducts = totalproducts;
    }

    public Long getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(Long totalprice) {
        this.totalprice = totalprice;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }
}

