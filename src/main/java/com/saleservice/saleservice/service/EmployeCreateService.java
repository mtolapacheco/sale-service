package com.saleservice.saleservice.service;

import com.saleservice.saleservice.input.EmployeeCreateInput;
import com.saleservice.saleservice.model.domain.Employee;
import com.saleservice.saleservice.model.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author marvin tola
 */
@Service
@Scope("prototype")
public class EmployeCreateService {

    private EmployeeCreateInput input;

    @Autowired
    private EmployeeRepository employeeRepository;

    private Employee employee;


    public void execute(){

        Employee employeeInstance=composeEmployeeInstance();
        employee = employeeRepository.save(employeeInstance);

    }

    private Employee composeEmployeeInstance() {

        Employee instance = new Employee();
        instance.setPosition(input.getPosition());
        instance.setFirstname(input.getFirstname());
        instance.setLastname(input.getLastname());
        instance.setEmail(input.getEmail());
        instance.setGender(input.getGender());
        instance.setIsdeleted(Boolean.TRUE);
        instance.setCreateddate(new Date());
        return instance;
    }

    public EmployeeCreateInput getInput() {
        return input;
    }

    public void setInput(EmployeeCreateInput input) {
        this.input = input;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
